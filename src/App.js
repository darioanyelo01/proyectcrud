import React, {useState} from 'react';
import UserTable from './components/UserTable';
import AddUserForm from './components/AddUserForm';
import EditYo from './components/EditYo';
import { v4 as uuidv4 } from 'uuid';

function App() {

  // Agregar usuarios
  // un array de objetos
  // con las propiedades id,name,username
  const usersData = [
    { id: uuidv4(), name: 'Pikachu', username: 'Electrico'},
    { id: uuidv4(), name: 'Charmeleon', username: 'Acuatico' },
    { id: uuidv4(), name: 'Squirtle', username: 'Normal' },
    { id: uuidv4(), name: 'Kakuma', username: 'Roca' },
    { id: uuidv4(), name: 'Pidgoutle', username: 'Volador' },
  ]
  // usuar los hoots state y pasamos el nombre 
  // del array userData
  const [users, setUsers] = useState(usersData)

  const addUser = (user) => {
    user.id = uuidv4()
    console.log(user)
    setUsers([
      ...users,
      user
    ])
  }

  // Eliminar usuario
  const deleteUser = id => {
    setUsers(users.filter(user => user.id !== id))
  }

  // Editar usuario
  const [editing, setEditing] = useState(false)

  const initialFormState = { id: null, name: '', username: '' }
  const [currentUser, setCurrentUser] = useState(initialFormState)

  const editRow = user => {
    setEditing(true) 
    setCurrentUser({ id: user.id, name: user.name, username: user.username })
  }

  const updateUser = (id, updatedUser) => {
    setEditing(false)
    setUsers(users.map(user => (user.id === id ? updatedUser : user)))
  }

  return (
    <div className="container">
      <h1>Aplicación CRUD en una APP en React</h1>
      <div className="flex-row">
        <div className="flex-large">
        {editing ? (
          <div>
            <h2>Editar </h2>
            <EditYo 
              setEditing={setEditing}
              currentUser={currentUser}
              updateUser={updateUser}
            />
          </div>
        ) : (
          <div>
            <h2>+ Ingresar nuevo datos</h2>
            <AddUserForm addUser={addUser}  />
          </div>
        )}
      </div>
        <div className="flex-large">
          <h2>Ver usuario</h2>
          <UserTable users={users} deleteUser={deleteUser} editRow={editRow} />
        </div>
      </div>
    </div>
  );
}

export default App;
